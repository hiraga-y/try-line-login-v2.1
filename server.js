require('dotenv').config();

const http = require('http');
const querystring = require('querystring');
const express = require('express');
const request = require('request');
const jwt = require('jsonwebtoken');
const app = express();

const config = {
  clientId: process.env.CLIENT_ID,
  clientSecret: process.env.CLIENT_SECRET,
  baseUrl: process.env.BASE_URL
};

app.use('/callback', (req, res) => {
  // アクセストークンを取得する
  const form = {
    'grant_type': 'authorization_code',
    'code': req.query.code,
    'redirect_uri': `${config.baseUrl}/callback`,
    'client_id': config.clientId,
    'client_secret': config.clientSecret
  };
  const url = 'https://api.line.me/oauth2/v2.1/token';
  request.post({ url, form }, (err, httpRes, body) => {
    // idtokenを利用してプロフィールを取得する
    const { id_token: idToken } = JSON.parse(body);
    const profile = jwt.verify(idToken, config.clientSecret);
    res.send(profile);
    
    // Social APIを使う場合
    // const { access_token: accessToken } = JSON.parse(body);
    // const options = {
    //   url: 'https://api.line.me/v2/profile',
    //   headers: {
    //     'Authorization': `Bearer ${accessToken}`
    //   }
    // };
    // request(options, (err, httpRes, body) => {
    //   res.send(body);
    // });
  })
});

app.use('/', (req, res) => {
  // LINEの認証画面にリダイレクトする
  const url = 'https://access.line.me/oauth2/v2.1/authorize';
  const query = querystring.stringify({
    'response_type': 'code',
    'client_id': config.clientId,
    'redirect_uri': `${config.baseUrl}/callback`,
    'state': 'statestringhere',
    'scope': 'profile openid'
  });
  
  return res.redirect(`${url}?${query}`);
});


const server = http.createServer(app);
server.listen(3000);
